#
# includes.sh
# Script to manage includes/sources for the shell
#
# Topics are split up by file some include functions as well as simple aliases
#
# to ENABLE any file for inclusion it MUST end with a *.on extension
# to DISABLE a file's inclusion it should have any other extension (typically *.off though)
#


##
## 0. define locations
##
DIR_DOTS='/home/indiv/.dots/'
DIR_INCLUDES="$DIR_DOTS/includes/"
DIR_FUNCTS="$DIR_DOTS/functs/"
DIR_SCRIPTS="$DIR_DOTS/scripts/"
DIR_APPS='/home/indiv/Applications/'
LAST_DIR=$PWD                       #remember "current" directory to return to

##
## 1. append to $PATH
##

PATH="$PATH:$DIR_SCRIPTS:$DIR_APPS"


##
## 2. Loop through the contents of above folders and 'source' any with *.on extensions
##

cd $DIR_INCLUDES
#printf "sourcing: "
for include_file in *.on
do
  #printf "$include_file, "
  source $include_file
done
unset include_file


##
## 3. return to orig directory and cleanup
##

cd $LAST_DIR
unset LAST_DIR



##
## x. re-init
##
alias source-includes="source $DIR_DOTS/includes.sh"
alias rl='echo ... && zsh &&  clear'
alias cls='clear'
alias q='exit || quit'

