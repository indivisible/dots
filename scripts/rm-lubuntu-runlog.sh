#!/bin/sh
# rm-lubuntu-runlog.sh
# clears and logs the logfile size

LOGFILE="$DIR_LOGS/rm-lubuntu-runlog.log"

TARGET_LOG_LXDE="$HOME/.cache/lxsession/Lubuntu/run.log"
TARGET_LOGS_JOURNAL="/var/log/journal/937b1c98468f4c448123e6b3df6fd27c/*@*.journal"

TIMESTAMP=$(date +"%Y-%m-%d_%H:%M:%S")

LOGFILE_LXDE_SIZE=$(stat -c%s "$TARGET_LOG_LXDE")
if [ -z $LOGFILE_LXDE_SIZE ]; then 
  LOGFILE_LXDE_SIZE=-1
fi # TODO: else rm

LOGFILES_JOURNAL_SIZE=$(du -ac ./*@*.journal | tail -1 | awk '{print $1;}')
if [ -z $LOGFILES_JOURNAL_SIZE ]; then
  LOGFILES_JOURNAL_SIZE=-1;
fi # TODO: else rm

rm $TARGET_LOG_LXDE
rm $TARGET_LOGS_JOURNAL


echo "$TIMESTAMP :: run.log was $LOGFILE_LXDE_SIZE bytes" >> $LOGFILE
echo "$TIMESTAMP :: journal was $LOGFILES_JOURNAL_SIZE"
