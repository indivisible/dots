## script to create and activate VirtualENV for the sarchive_backend project

cd $DIR_SARCHIVE_B        # defined in ../includes/folders.on
pwd

python3.7 -m venv .venv
. .venv/bin/activate
poetry install

