# touchpad-toggle
# disable/enable the touchpad to reactivate it
#
# inspiration from: 
#  https://askubuntu.com/questions/528293/is-there-a-way-to-restart-the-touchpad-driver


PAD_NAME='SynPS/2 Synaptics TouchPad'


echo "Toggling touchpad: $PAD_NAME..."
xinput disable "$PAD_NAME"
xinput enable "$PAD_NAME"
echo done

