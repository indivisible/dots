#!/usr/bin/env bash
# script to periodically clear out stale temporary files


# files YOUNGER than this value (mins) are ignored as they may still be required
MIN_FILE_AGE=20

# declare the folders to empty out
declare -a TMP_FOLDERS=(       \
  '/Users/david/tmp/cache_0'   \
  '/Users/david/tmp/cache_1'   \
  '/Users/david/tmp/cache_2'   \
)



# function that delete files in a folder based off created time
function cleanfolder {

  for file in "$1"/*; do
    echo "$file"
  done

}





for cache in "${TMP_FOLDERS[@]}"; do
  echo "Folder: $cache"
  cleanfolder "$cache"
done

echo "All done."
